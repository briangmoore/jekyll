---
layout: post
title:  "Here's some content"
date:   2017-11-02 15:35:00 -0800
categories: jekyll content
---

Need a git client.  Linux/Mac command line git, or on Windows install the basic client from (https://git-scm.com)

You have to add an ssh key (for each client machine/account) to your profile on gitlab.sdstate.edu,
Profile settings --> SSH Keys.  

Then (for example) in git client

```
git clone git@gitlab.sdstate.edu:Brian.Moore/cluster-software.git
cd cluster-software
```

Need to set name and email at least once.  See also  (https://git-scm.com/book/en/v1/Getting-Started-First-Time-Git-Setup).

```
git config --global user.name "brian.moore"
git config --global user.email "brian.moore@sdstate.edu"
```

Now, if you add or edit content, you go through the three commands, `add`, `commit`, and `push` to upload the new content to the server.  Let's say we just create a new file then add/commit/push:

```console
brian@SASQUATCHVM MINGW64 ~/cluster-software (master)
$ touch testfile

brian@SASQUATCHVM MINGW64 ~/cluster-software (master)
$ git add testfile

brian@SASQUATCHVM MINGW64 ~/cluster-software (master)
$ git commit -m 'added testfile'
[master 3412aa1] added testfile
 1 file changed, 0 insertions(+), 0 deletions(-)
 create mode 100644 testfile

brian@SASQUATCHVM MINGW64 ~/cluster-software (master)
$ git push
Ubuntu 12.04.1 LTS -- Gitlab Server
********************************************************************
*                                                                  *
* This system is for the use of SDSU authorized users only.  Usage *
* of this system may be monitored and recorded by system personnel.*
*                                                                  *
* Anyone using this system expressly consents to such monitoring   *
* and is advised that if such monitoring reveals possible          *
* evidence of criminal activity, system personnel may provide the  *
* evidence from such monitoring to law enforcement officials.      *
*                                                                  *
********************************************************************
Counting objects: 3, done.
Delta compression using up to 2 threads.
Compressing objects: 100% (2/2), done.
Writing objects: 100% (3/3), 325 bytes | 162.00 KiB/s, done.
Total 3 (delta 0), reused 1 (delta 0)
To gitlab.sdstate.edu:Brian.Moore/cluster-software.git
   8d062a6..3412aa1  master -> master

brian@SASQUATCHVM MINGW64 ~/cluster-software (master)
$
```

[Brian Moore](https://www.sdstate.edu/directory/brian-moore)
